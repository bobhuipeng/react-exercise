import { connect } from 'react-redux'

import NewUser from '../components/NewUser'
import { updateUser, addUser } from '../actions/Actions'

//TODO: this function need improve. 
const getMaxId = (users) => {
  if (users !== undefined && users.length > 0) {
    return Math.max(...users.map(user => user.id))
  } else {
    //TODO: this may cause id conflict. 
    return 0;
  }

}

const mapStateToProps = state => ({
  nextId: getMaxId(state.users) + 1,
  userInfo: state.editedUser
})


const mapDispatchToProps = dispatch => ({

  updateUser(userInfo) {
    dispatch(
      updateUser(userInfo)
    )
  },
  addNewUser(userInfo) {
    dispatch(
      addUser(userInfo)
    )
  },
})

export default connect(mapStateToProps, mapDispatchToProps)(NewUser)