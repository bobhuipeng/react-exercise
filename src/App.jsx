import React, { Component } from "react";
import "./App.css";

import UserManagement from './components/UserManagement'

import { Container } from 'reactstrap';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Container>
          <UserManagement />
        </Container>
      </div>
    );
  }
}

export default App;
