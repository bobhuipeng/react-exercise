
const constants = {
  
  ADD_NEW_USER:'ADD_NEW_USER',
  UPDATE_USER:'UPDATE_USER',
  UPDATE_EDITED_USER:'UPDATE_EDITED_USER',
  SEARCH_USER: 'SEARCH_USER',

}

export default constants;