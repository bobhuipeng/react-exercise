import CTS from './Constants'

const actionGenerator = (actionType) => {
  return (payload)=>({
    type: actionType,
    error:null,
    payload:payload,
  })
}


export const addUser = userInfo => (dispatch, getState) => {
 //TODO: add exception ctrl
  if(getState().users !== null && getState().users !== undefined){
    localStorage.setItem('userList',JSON.stringify( [...getState().users,userInfo]) );
  }

  dispatch(actionGenerator(CTS.ADD_NEW_USER)(userInfo))
  
}

export const updateEditedUser = actionGenerator(CTS.UPDATE_EDITED_USER)
export const updateUser = userInfo => (dispatch, getState) => {
 //TODO: add exception ctrl
  let users = getState().users.map(it =>
    (it.id === userInfo.id)
      ? userInfo
      : it
  )

  localStorage.setItem('userList',JSON.stringify(users ) );

  dispatch(actionGenerator(CTS.UPDATE_USER)(userInfo))
  
}
export const searchUser = () =>  (dispatch, getState) =>{
  //TODO: add exception ctrl
   let users =JSON.parse(localStorage.getItem('userList'));
   users = users === null || users === undefined ? [] : users
   dispatch(actionGenerator(CTS.SEARCH_USER)(users))
  }