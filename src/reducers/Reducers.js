import {
  combineReducers
} from 'redux'

import editedUser from './EditUserReducer'
import users from './UserListReducer'

export default combineReducers({
  users,
  editedUser,
})