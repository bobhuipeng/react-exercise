import CST from '../actions/Constants'

const editedUser = (state = null, action) => {

  if (action.type === CST.UPDATE_EDITED_USER) {
    return action.payload
  } else {
    return state
  }
}



export default editedUser