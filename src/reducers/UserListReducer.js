import CST from '../actions/Constants'

const userList = (state = [], action) => {
  switch (action.type) {
    case CST.ADD_NEW_USER:
      return [...state, action.payload]; // add user to list
    case CST.UPDATE_USER:
      return updateUser(state, action.payload); // update user in list
    case CST.SEARCH_USER:
      return action.payload; // all user list 
    default:
      return state;
  }
}

const updateUser = (state, user) => state.map(it =>
  (it.id === user.id) ?
  user :
  it
)



export default userList