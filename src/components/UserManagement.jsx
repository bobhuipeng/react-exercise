import React, { Component } from 'react'

import NewUser from '../containers/AddNewUser'
import UserList from '../containers/UserList'

export default class UserManagement extends Component {

  render() {
    return (
      <div>
        <NewUser />
        <UserList />
      </div>
    )
  }
}
