import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Table, Input, Button, ButtonGroup } from 'reactstrap';

export default class NewUser extends Component {
  static propTypes = {
    userInfo: PropTypes.object,
    updateUser: PropTypes.func.isRequired

  }

  constructor(props) {
    super(props)
    if (this.props.userInfo !== undefined && this.props.userInfo !== null) {
      this.state = {
        ...this.props.userInfo
      }
    } else {
      this.state = this.userInfo = {
        id: null,
        firstName: '',
        lastName: '',
        phoneNo: ''
      }
    }

  }

  componentDidUpdate(previousProps, previousState) {

    if (previousProps.userInfo !== this.props.userInfo) {
      this.setState({
        id: this.props.userInfo.id,
        firstName: this.props.userInfo.firstName,
        lastName: this.props.userInfo.lastName,
        phoneNo: this.props.userInfo.phoneNo
      })
    }

  }

  updateFirstName = (e) => {
    this.setState({ firstName: e.target.value })
  }

  updateLastName = (e) => {
    this.setState({ lastName: e.target.value })
  }

  updatePhoneNo = (e) => {
    this.setState({ phoneNo: e.target.value })
  }

  updateUser = () => {

    if (this.state.id !== undefined && this.state.id !== null) {
      this.props.updateUser(this.createNewUser(this.props.userInfo.id))
    } else {
      this.props.addNewUser(this.createNewUser(this.props.nextId))
    }

  }

  createNewUser = (id) => {
    return {
      id: id,
      firstName: this.state.firstName,
      lastName: this.state.lastName,
      phoneNo: this.state.phoneNo
    }
  }

  resetValues = () => {
    this.setState({
      id: this.props.userInfo.id,
      firstName: this.props.userInfo.firstName,
      lastName: this.props.userInfo.lastName,
      phoneNo: this.props.userInfo.phoneNo
    })
  }

  addNewUser = () => {
    this.setState({
      id: null,
      firstName: '',
      lastName: '',
      phoneNo: ''
    })
  }

  render() {
    return (
      <div>
        <Table>
          <thead>
            <tr>
              <th>First Name</th>
              <th>Last Name</th>
              <th>Phone No</th>
            </tr>
          </thead>
          <tbody>

            <tr>
              <td><Input onChange={this.updateFirstName} value={this.state.firstName} /> </td>
              <td><Input onChange={this.updateLastName} value={this.state.lastName} /> </td>
              <td><Input onChange={this.updatePhoneNo} value={this.state.phoneNo} /> </td>
            </tr>
          </tbody>
        </Table>
        <ButtonGroup className="btn float-lest">
          <Button onClick={this.addNewUser}>Add New User</Button>
        </ButtonGroup>
        <ButtonGroup className="btn float-right">
          <Button onClick={this.updateUser}>Save</Button>
          <Button onClick={this.resetValues}>Cancel</Button>
        </ButtonGroup>

      </div>
    )
  }
}
