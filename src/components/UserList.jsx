import React, { Component } from 'react'
import PropTypes from 'prop-types'

import { Button } from 'reactstrap';

import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';

export default class UserList extends Component {
  static propTypes = {
    users: PropTypes.array.isRequired,
    updateEditedUser: PropTypes.func.isRequired,
    searchUser: PropTypes.func.isRequired,
  }

  componentDidMount() {
    this.props.searchUser()
  }

  activeFormatter = (cell, row, enumObject, index) => {
    return (
      <Button onClick={() => this.editUser(row.id)}>Edit</Button>
    );
  }

  editUser = (id) => {
    //TODO: improve user find
    let user = this.props.users.filter((e) => e.id === id)[0]

    this.props.updateEditedUser({
      id: user.id,
      firstName: user.firstName,
      lastName: user.lastName,
      phoneNo: user.phoneNo
    })
  }
  render() {
    return (
      <div>
        <BootstrapTable data={this.props.users}>
          <TableHeaderColumn dataField='id' isKey width='9%'>ID</TableHeaderColumn>
          <TableHeaderColumn dataField='firstName' filter={{ type: 'TextFilter' }} dataSort={true} width='28%'>First Name</TableHeaderColumn>
          <TableHeaderColumn dataField='lastName' filter={{ type: 'TextFilter' }} dataSort={true} width='28%'>Last Name</TableHeaderColumn>
          <TableHeaderColumn dataField='phoneNo' filter={{ type: 'TextFilter' }} dataSort={true} width='28%'>Phone No</TableHeaderColumn>
          <TableHeaderColumn dataField='active' dataFormat={this.activeFormatter}></TableHeaderColumn>
        </BootstrapTable>
      </div>
    )
  }
}
