import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import { composeWithDevTools } from 'redux-devtools-extension';

import reducers from './reducers/Reducers'

const store = createStore(reducers,composeWithDevTools(applyMiddleware(thunk)))

ReactDOM.render(
<Provider store = {store}>
    <App />
</Provider>
, document.getElementById('root'));
registerServiceWorker();
